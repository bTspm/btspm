require 'spec_helper'
require 'btspm/app_startup'

RSpec.describe Btspm::AppStartup do

  describe '#establish_env_variables_hash' do
    it 'should return hash with env variables' do
      path = 'config/env-props.yml'
      response = Btspm::AppStartup.new(config_file_path: path, environment: Rails.env).establish_env_variables_hash
      expect(response).to include Rails.env => 'Testing Startup Env'
    end
  end

end