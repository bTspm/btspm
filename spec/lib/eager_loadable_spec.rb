require 'spec_helper'
require 'btspm/eager_loadable'

RSpec.describe Btspm::EagerLoadable do

  module Btspm
    class Dummy
    end
  end

  let!(:loader) {Btspm::Dummy.new.extend(Btspm::EagerLoadable)}

  describe '#eager_load' do
    it 'should return collection with pre-loaded associations for active_record_relation' do
      user = User.create
      user.posts = [Post.create]

      #Activerecord relation
      record_relation = loader.eager_load(User.where(id: user.id), :posts)
      expect(record_relation.first.association(:posts).loaded?).to be true

      #Array
      array = loader.eager_load([user], :posts)
      expect(array.first.association(:posts).loaded?).to be true

      #Single record
      record = loader.eager_load(user, :posts)
      expect(record.association(:posts).loaded?).to be true
    end
  end

end