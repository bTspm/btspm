require 'rubygems'

ENV['RAILS_ENV'] ||= 'test'

require File.expand_path('../dummy/config/environment.rb',  __FILE__)
require 'rspec/rails'

Rails.backtrace_cleaner.remove_silencers!
# Load spec_support files
Dir["#{File.dirname(__FILE__)}/spec_support/**/*.rb"].each { |f| require f }
Dir["#{File.dirname(__FILE__)}/support/**/*.rb"].each { |f| require f }

require 'simplecov'
SimpleCov.start

RSpec.configure do |config|
  config.mock_with :rspec
  config.use_transactional_fixtures = true
  config.infer_base_class_for_anonymous_controllers = false
  config.infer_spec_type_from_file_location!
  config.order = 'random'
end

require 'btspm/app_startup'

ENV_PROPS = Btspm::AppStartup.new(config_file_path: ("#{File.dirname(__FILE__)}/dummy/config/env-props.yml"), environment: Rails.env).establish_env_variables_hash


