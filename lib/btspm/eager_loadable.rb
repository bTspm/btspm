module Btspm
  module EagerLoadable

    def eager_load(collection, *associations)
      EagerLoader.load(collection, *associations)
    end

    class EagerLoader
      def initialize(coll, *assocs)
        @collection = coll
        @associations = assocs
      end

      def self.load(collection, *associations)
        preloader = new(collection, *associations)
        preloader.load
        collection
      end

      def load
        preloader = ActiveRecord::Associations::Preloader.new
        preloader.preload(collection, associations)
      end

      private

      def true_collection?
        !@collection.is_a?(ActiveRecord::Base)
      end

      def collection
        true_collection? ? @collection : [@collection]
      end

      attr_reader :associations
    end
  end
end