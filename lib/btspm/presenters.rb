module Btspm
  module Presenters
    module Helpers

      def present(model, presenter_class = nil, locals = {})
        presenter    = presenter_class || get_presenter_class(model)
        view_context = self.kind_of?(ActionController::Base) ? self.view_context : self
        presenter.present(model, view_context, locals)
      end

      private

      def get_presenter_class(model)
        klass = if model.is_a?(ActiveRecord::Relation)
                  model.klass
                elsif model.respond_to?(:each)
                  model.first.class
                else
                  model.class
                end
        "#{klass.name.pluralize.underscore}_presenter".classify.constantize
      end
    end

    [:action_controller, :action_view].each do |cv|
      ActiveSupport.on_load(cv) do
        include Helpers
      end
    end

    module Presentable
      def self.included(mod)
        mod.extend ClassMethods
      end

      module ClassMethods
        def present(data_object, view, locals = {})
          klass = is_enumerable?(data_object) ? :Enum : :Scalar
          self.const_get(klass).new(data_object, view, locals)
        end

        private

        def is_enumerable?(data_object)
          data_object.is_a?(Array) || (data_object.is_a?(ActiveRecord::Relation) rescue false)
        end
      end
    end

    class ScalarPresenter < SimpleDelegator
      def initialize(data_object, view, locals = {})
        @view = view
        @original_locals = locals
        create_instance_variables(locals)
        super(data_object)
      end

      def data_object
        __getobj__
      end

      def h
        @view
      end

      private

      def create_instance_variables(locals)
        locals.each do |k,v|
          instance_variable_set("@#{k}", v)
        end
      end
    end

    class EnumPresenter < ScalarPresenter
      include Enumerable

      def each
        data_object.each do |datum|
          yield self.class.parent.present(datum, h, @original_locals)
        end
      end
    end
  end
end
