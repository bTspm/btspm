require 'btspm/presenters'
require 'btspm/eager_loadable'
require 'btspm/app_startup'

module Btspm
  class Engine < ::Rails::Engine
    isolate_namespace Btspm
  end
end
