$:.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'btspm/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'btspm'
  s.version     = Btspm::VERSION
  s.authors     = ['bTspm']
  s.email       = ['bTspm10@gmail.com']
  # s.homepage    = 'TODO'
  s.summary     = 'Btspm gem'
  s.description = 'Currently has presenters and eager_loads'
  s.license     = 'MIT'

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.rdoc']
  s.test_files = Dir['spec/**/*']

  s.add_runtime_dependency 'rails'

  s.add_development_dependency 'sqlite3'
  s.add_dependency 'rspec-rails'
  s.add_dependency 'tzinfo-data'
  s.add_dependency 'simplecov'
end
